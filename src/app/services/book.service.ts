import { Injectable } from '@angular/core';
import { HttpClient , HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private httpClient:HttpClient) { }

  getBooks(){
    return this.httpClient.get('https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=Clh8dH3XopGRTjNG0rT2bvSuzpozDgAQ');
  }

  getBooks_reviews(data:any){

    let params = new HttpParams()
    .set('title',data.title ? data.title : '')
    .set('isbns',data.isbns[0].isbn13 ? data.isbns[0].isbn13 : '')
    .set('author',data.author ? data.author : '')

    return this.httpClient.get('https://api.nytimes.com/svc/books/v3/reviews.json?api-key=Clh8dH3XopGRTjNG0rT2bvSuzpozDgAQ',{params:params});
  }

}
