import { Component, OnInit } from '@angular/core';
import {BookService } from '../services/book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  constructor(public booksService:BookService) { }
  bookData : any

  ngOnInit(): void {
    this.list();

  }

  list(){
    this.booksService.getBooks().subscribe(
      (res:any)=>{
      this.bookData = res.results.books
    },
    (err)=>{
      console.error(err)
    }
    )
  }


  review(data:any){
    this.booksService.getBooks_reviews(data).subscribe((res:any)=>{
    console.log("review",res)
    },
    (err)=>{
      console.error(err)
    })
  }

}
